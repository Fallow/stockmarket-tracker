require("dotenv").config();

const user = process.env.DB_USER;
const pass = process.env.DB_PASS;
module.exports = {
  mongoURI: `mongodb+srv://${user}:${pass}@cluster0-znzhq.mongodb.net/test?retryWrites=true&w=majority`,
  secretOrKey: "secret"
};
